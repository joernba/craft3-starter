# Changelog

## 1.0.3 2018-11-05
### Changed
- removed duplicate type in composer.json

## 1.0.3 2018-11-02
### Changed
- trying to use git flow again

## 1.0.2 2018-11-02
### Changed
- trying to use git flow

## 1.0.1 2018-11-02
### Changed
- added type to composer.json to avoid error with stability when installing

## 1.0.0 2018-11-02
### Added
- Initial Release
