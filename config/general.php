<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 */

return [
    // Global settings
    '*' => [
        // Default Week Start Day (0 = Sunday, 1 = Monday...)
        'defaultWeekStartDay' => 1,

        // Enable CSRF Protection (recommended)
        'enableCsrfProtection' => true,

        // Whether "index.php" should be visible in URLs
        'omitScriptNameInUrls' => true,

        // Control Panel trigger word
        'cpTrigger' => 'admin',

        // The secure key Craft will use for hashing and encrypting data
        'securityKey' => getenv('SECURITY_KEY'),

        // enable dev mode on local
        'devMode' => ($_SERVER['SERVER_NAME'] == 'local.test' ? true : false),

        // disable analytics on local
        'doAnalytics' => false,

        // set max file size
        'maxUploadFileSize' => 33554432,
    ],

    // Dev environment settings
    'dev' => [
        'imageDriver' => 'imagick',
        'siteUrl' => [
            'en_us' => 'http://'.$_SERVER['SERVER_NAME'].'/en/'
        ],
        'aliases' => [
            '@baseUrl'  => 'http://'.$_SERVER['SERVER_NAME'].'/',
            '@basePath' => '/Users/joern/Sites/valet/local/public/',
        ]
    ],

    // Staging environment settings
    'staging' => [
        'imageDriver' => 'imagick',
        'siteUrl' => [
            'en_us' => 'http://'.$_SERVER['SERVER_NAME'].'/en/'
        ],
        'aliases' => [
            '@baseUrl'  => 'http://'.$_SERVER['SERVER_NAME'].'/',
            '@basePath' => '/Users/joern/Sites/valet/local/public/',
        ]
    ],

    // Production environment settings
    'production' => [
        // Base site URL
        'siteUrl' => null,
    ],
];
